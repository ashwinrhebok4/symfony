<?php
// src/AppBundle/Controller/LuckyController.php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class LuckyController extends Controller
{
    /**
     * @Route("/project/home")
     */
    public function numberAction()
    {
        $number = random_int(0, 100);

        return $this->render('project/home.php.twig', array (
            'number' => $number,
            )
        );
    }
}