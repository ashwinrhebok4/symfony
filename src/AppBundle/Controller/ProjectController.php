<?php
/**
 * Created by PhpStorm.
 * User: 302638338
 * Date: 11/7/2018
 * Time: 2:07 PM
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ProjectController extends Controller
{
    /**
     * @Route("/project/{projectName}")
     */
    public function showAction($projectName)
    {
        $templating = $this->container->get('templating');
        return $this->render('project/show.html.twig', [
                 'name' => $projectName
        ]);
       return new Response($html);
    }
}